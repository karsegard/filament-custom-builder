<?php

namespace KDA\Filament\CustomBuilder;

use Filament\Forms\ComponentContainer;
use Filament\Forms\Components\Builder;
use Illuminate\View\View;
use KDA\Filament\Blocks\Facades\BlockManager;

class CustomBuilder extends Builder
{
    protected string $view = 'filament-custom-builder::custom-builder';
    
    protected bool $disableDrag = true;

    public function isDragDisabled(){
        return $this->evaluate($this->disableDrag);
    }

    public function getBlocksByGroup(): array
    {
        return collect($this->getBlocks())->mapWithKeys(function ($block, $key) {
            $block->by_group = method_exists($block, 'getGroup') ? $block->getGroup() : '';
            return [$key => $block];
        })->groupBy('by_group')->sortKeys()->toArray();
    }

    public function collapsedContent(ComponentContainer $container): View|string
    {
        $name = $container->getParentComponent()->getName();
        $block = BlockManager::getBlockByName($name);
        
        if ($block) {
            
            // dump(method_exists($block,'collapsedContent'));
            if (is_callable("$block::collapsedContent")) {
                $state = null;
                if ($statePath = $container->getStatePath()) {
                    $state= data_get($state, $statePath) ?? [];
                }
             //   dump($state);
                return $block::collapsedContent($state);
            }
        }
        return __('forms::components.builder.collapsed');
        /* if (! $view = $this->evaluate($this->renderInView)) {
            return __('renderInView not set or null');
        }

        try {
            return view(
                $view,
                ['preview' => $container->getParentComponent()->renderDisplay($container->getState())]
            );
        } catch (ErrorException|Exception $e) {
            return __('Error when rendering: :phError', ['phError' => $e->getMessage()]);
        }*/
    }
}
