<?php

namespace KDA\Filament\CustomBuilder;

use Filament\Forms\Components\Builder\Block;
use Filament\Forms\Components\Concerns\HasHint;

class CustomBlock extends Block {

    use HasHint;
    
    protected string | HtmlString | Closure | null $group = null;

    public function group(  string | HtmlString | Closure | null $group):static 
    {
        $this->group = $group;
        return $this;
    }

    public function getGroup():string | null
    {
        return $this->evaluate($this->group);
    }
    
}