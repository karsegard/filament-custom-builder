<x-dynamic-component :component="$getFieldWrapperView()" :id="$getId()" :label="$getLabel()" :label-sr-only="$isLabelHidden()" :helper-text="$getHelperText()"
    :hint="$getHint()" :hint-action="$getHintAction()" :hint-color="$getHintColor()" :hint-icon="$getHintIcon()" :required="$isRequired()" :state-path="$getStatePath()">
    @php
        $containers = $getChildComponentContainers();
        $isCloneable = $isCloneable();
        $isCollapsible = $isCollapsible();
        $isItemCreationDisabled = $isItemCreationDisabled();
        $isItemDeletionDisabled = $isItemDeletionDisabled();
        $isItemMovementDisabled = $isItemMovementDisabled();
    @endphp

    <div>
        @if (count($containers) > 1 && $isCollapsible)
            <div class="space-x-2 rtl:space-x-reverse" x-data="{}">
                <x-forms::link x-on:click="$dispatch('builder-collapse', '{{ $getStatePath() }}')" tag="button"
                    size="sm">
                    {{ __('forms::components.builder.buttons.collapse_all.label') }}
                </x-forms::link>

                <x-forms::link x-on:click="$dispatch('builder-expand', '{{ $getStatePath() }}')" tag="button"
                    size="sm">
                    {{ __('forms::components.builder.buttons.expand_all.label') }}
                </x-forms::link>
            </div>
        @endif
    </div>

    <div
        {{ $attributes->merge($getExtraAttributes())->class([
                'filament-forms-builder-component space-y-6 rounded-xl',
                'bg-gray-50 p-6' => $isInset(),
                'dark:bg-gray-500/10' => $isInset() && config('forms.dark_mode'),
            ]) }}>
        @if (count($containers))
            <ul class="space-y-6" wire:sortable
                wire:end.stop="dispatchFormEvent('builder::moveItems', '{{ $getStatePath() }}', $event.target.sortable.toArray())">
                @php
                    $hasBlockLabels = $hasBlockLabels();
                    $hasBlockNumbers = $hasBlockNumbers();
                @endphp

                @foreach ($containers as $uuid => $item)
                    <li x-data="{
                        isCreateButtonVisible: false,
                        isCollapsed: @js($isCollapsed()),
                    }"
                        x-on:builder-collapse.window="$event.detail === '{{ $getStatePath() }}' && (isCollapsed = true)"
                        x-on:builder-expand.window="$event.detail === '{{ $getStatePath() }}' && (isCollapsed = false)"
                        x-on:click="isCreateButtonVisible = true" x-on:mouseenter="isCreateButtonVisible = true"
                        x-on:click.away="isCreateButtonVisible = false" x-on:mouseleave="isCreateButtonVisible = false"
                        wire:key="{{ $this->id }}.{{ $item->getStatePath() }}.item"
                        wire:sortable.item="{{ $uuid }}"
                        x-on:expand-concealing-component.window="
                            error = $el.querySelector('[data-validation-error]')

                            if (! error) {
                                return
                            }

                            isCollapsed = false

                            if (document.body.querySelector('[data-validation-error]') !== error) {
                                return
                            }

                            setTimeout(() => $el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' }), 200)
                        "
                        @class([
                            'bg-white border border-gray-300 shadow-sm rounded-xl relative flex flex-row',
                            'dark:bg-gray-800 dark:border-gray-600' => config('forms.dark_mode'),
                        ])>
                        @if (!$isItemMovementDisabled || $hasBlockLabels || !$isItemDeletionDisabled || $isCollapsible || $isCloneable)
                         {{--   <header @class([
                                'flex items-center h-10 overflow-hidden border-b bg-gray-50 rounded-t-xl',
                                'dark:bg-gray-800 dark:border-gray-700' => config('forms.dark_mode'),
                            ])>
                                @unless($isItemMovementDisabled)
                                    <button title="{{ __('forms::components.builder.buttons.move_item.label') }}"
                                        wire:sortable.handle
                                        wire:keydown.prevent.arrow-up="dispatchFormEvent('builder::moveItemUp', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                        wire:keydown.prevent.arrow-down="dispatchFormEvent('builder::moveItemDown', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                        type="button" @class([
                                            'flex items-center justify-center flex-none w-10 h-10 text-gray-400 border-r rtl:border-l rtl:border-r-0 transition hover:text-gray-500',
                                            'dark:border-gray-700' => config('forms.dark_mode'),
                                        ])>
                                        <span class="sr-only">
                                            {{ __('forms::components.builder.buttons.move_item.label') }}
                                        </span>

                                        <x-heroicon-s-switch-vertical class="w-4 h-4" />
                                    </button>
                                @endunless

                                @if ($hasBlockLabels)
                                    <p @class([
                                        'flex-none px-4 text-xs font-medium text-gray-600 truncate',
                                        'dark:text-gray-400' => config('forms.dark_mode'),
                                    ])>
                                        @php
                                            $block = $item->getParentComponent();
                                            
                                            $block->labelState($item->getRawState());
                                        @endphp

                                        {{ $item->getParentComponent()->getLabel() }}

                                        @php
                                            $block->labelState(null);
                                        @endphp

                                        @if ($hasBlockNumbers)
                                            <small class="font-mono">{{ $loop->iteration }}</small>
                                        @endif
                                    </p>
                                @endif

                                <div class="flex-1"></div>

                                <ul @class([
                                    'flex divide-x rtl:divide-x-reverse',
                                    'dark:divide-gray-700' => config('forms.dark_mode'),
                                ])>
                                    @if ($isCloneable)
                                        <li>
                                            <button
                                                title="{{ __('forms::components.builder.buttons.clone_item.label') }}"
                                                wire:click="dispatchFormEvent('builder::cloneItem', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                                type="button" @class([
                                                    'flex items-center justify-center flex-none w-10 h-10 text-gray-400 transition hover:text-gray-500',
                                                    'dark:border-gray-700' => config('forms.dark_mode'),
                                                ])>
                                                <span class="sr-only">
                                                    {{ __('forms::components.builder.buttons.clone_item.label') }}
                                                </span>

                                                <x-heroicon-s-duplicate class="w-4 h-4" />
                                            </button>
                                        </li>
                                    @endif

                                    @unless($isItemDeletionDisabled)
                                        <li>
                                            <button title="{{ __('forms::components.builder.buttons.delete_item.label') }}"
                                                wire:click="dispatchFormEvent('builder::deleteItem', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                                type="button" @class([
                                                    'flex items-center justify-center flex-none w-10 h-10 text-danger-600 transition hover:text-danger-500',
                                                    'dark:text-danger-500 dark:hover:text-danger-400' => config(
                                                        'forms.dark_mode'
                                                    ),
                                                ])>
                                                <span class="sr-only">
                                                    {{ __('forms::components.builder.buttons.delete_item.label') }}
                                                </span>

                                                <x-heroicon-s-trash class="w-4 h-4" />
                                            </button>
                                        </li>
                                    @endunless

                                    @if ($isCollapsible)
                                        <li>
                                            <button
                                                x-bind:title="(!isCollapsed) ?
                                                '{{ __('forms::components.builder.buttons.collapse_item.label') }}' :
                                                '{{ __('forms::components.builder.buttons.expand_item.label') }}'"
                                                x-on:click="isCollapsed = ! isCollapsed" type="button"
                                                class="flex items-center justify-center flex-none w-10 h-10 text-gray-400 transition hover:text-gray-500">
                                                <x-heroicon-s-minus-sm class="w-4 h-4" x-show="! isCollapsed" />

                                                <span class="sr-only" x-show="! isCollapsed">
                                                    {{ __('forms::components.builder.buttons.collapse_item.label') }}
                                                </span>

                                                <x-heroicon-s-plus-sm class="w-4 h-4" x-show="isCollapsed" x-cloak />

                                                <span class="sr-only" x-show="isCollapsed" x-cloak>
                                                    {{ __('forms::components.builder.buttons.expand_item.label') }}
                                                </span>
                                            </button>
                                        </li>
                                    @endif
                                </ul>
                            </header>--}}
                        @endif
                           
                            <div class="p-6 flex-1" x-show="! isCollapsed">
                                @if ($hasBlockLabels)
                                    <p @class([
                                        'flex-none  text-xs font-medium text-gray-600 truncate',
                                        'dark:text-gray-400' => config('forms.dark_mode'),
                                    ])>
                                        @php
                                            $block = $item->getParentComponent();
                                            
                                            $block->labelState($item->getRawState());
                                        @endphp

                                        {{ $item->getParentComponent()->getLabel() }}

                                        @php
                                            $block->labelState(null);
                                        @endphp

                                        @if ($hasBlockNumbers)
                                            <small class="font-mono">{{ $loop->iteration }}</small>
                                        @endif
                                    </p>
                                @endif
                                {{ $item }}
                            </div>
                            <div class="p-2 text-xs flex-1 text-center text-gray-400" x-show="isCollapsed" x-cloak>
                                {!! $collapsedContent($item) !!}
                            </div>
                            <div class="flex-none w-10 ">
                                @unless($isItemMovementDisabled)
                                @if(!$isDragDisabled())
                                <button title="{{ __('forms::components.builder.buttons.move_item.label') }}"
                                    wire:sortable.handle
                                    wire:keydown.prevent.arrow-up="dispatchFormEvent('builder::moveItemUp', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                    wire:keydown.prevent.arrow-down="dispatchFormEvent('builder::moveItemDown', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                    type="button" @class([
                                        'flex items-center justify-center flex-none w-10 h-10 text-gray-400 border-r rtl:border-l rtl:border-r-0 transition hover:text-gray-500',
                                        'dark:border-gray-700' => config('forms.dark_mode'),
                                    ])>
                                    <span class="sr-only">
                                        {{ __('forms::components.builder.buttons.move_item.label') }}
                                    </span>

                                    <x-heroicon-s-switch-vertical class="w-4 h-4" />
                                </button>
                                @endif
                                <button title="{{ __('forms::components.builder.buttons.move_item.label') }}"
                                    
                                    wire:click="dispatchFormEvent('builder::moveItemUp', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                    type="button" @class([
                                        'flex items-center justify-center flex-none w-10 h-10 text-gray-400 border-r rtl:border-l rtl:border-r-0 transition hover:text-gray-500',
                                        'dark:border-gray-700' => config('forms.dark_mode'),
                                    ])>
                                    <span class="sr-only">
                                        {{ __('forms::components.builder.buttons.move_item.label') }}
                                    </span>

                                    <x-heroicon-s-arrow-narrow-up class="w-4 h-4" />
                                </button>
                                <button title="{{ __('forms::components.builder.buttons.move_item.label') }}"
                                    
                                    wire:click="dispatchFormEvent('builder::moveItemDown', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                    type="button" @class([
                                        'flex items-center justify-center flex-none w-10 h-10 text-gray-400 border-r rtl:border-l rtl:border-r-0 transition hover:text-gray-500',
                                        'dark:border-gray-700' => config('forms.dark_mode'),
                                    ])>
                                    <span class="sr-only">
                                        {{ __('forms::components.builder.buttons.move_item.label') }}
                                    </span>

                                    <x-heroicon-s-arrow-narrow-down class="w-4 h-4" />
                                </button>
                                
                            @endunless
                                @unless($isItemDeletionDisabled)
                                    <button title="{{ __('forms::components.builder.buttons.delete_item.label') }}"
                                        wire:click="dispatchFormEvent('builder::deleteItem', '{{ $getStatePath() }}', '{{ $uuid }}')"
                                        type="button" @class([
                                            'flex items-center justify-center flex-none w-10 h-10 text-danger-600 transition hover:text-danger-500',
                                            'dark:text-danger-500 dark:hover:text-danger-400' => config(
                                                'forms.dark_mode'
                                            ),
                                        ])>
                                        <span class="sr-only">
                                            {{ __('forms::components.builder.buttons.delete_item.label') }}
                                        </span>

                                        <x-heroicon-s-trash class="w-4 h-4" />
                                    </button>
                            @endunless
                                @if ($isCollapsible)
                                      
                                            <button
                                                x-bind:title="(!isCollapsed) ?
                                                '{{ __('forms::components.builder.buttons.collapse_item.label') }}' :
                                                '{{ __('forms::components.builder.buttons.expand_item.label') }}'"
                                                x-on:click="isCollapsed = ! isCollapsed" type="button"
                                                class="flex items-center justify-center flex-none w-10 h-10 text-gray-400 transition hover:text-gray-500">
                                                <x-heroicon-s-minus-sm class="w-4 h-4" x-show="! isCollapsed" />

                                                <span class="sr-only" x-show="! isCollapsed">
                                                    {{ __('forms::components.builder.buttons.collapse_item.label') }}
                                                </span>

                                                <x-heroicon-s-plus-sm class="w-4 h-4" x-show="isCollapsed" x-cloak />

                                                <span class="sr-only" x-show="isCollapsed" x-cloak>
                                                    {{ __('forms::components.builder.buttons.expand_item.label') }}
                                                </span>
                                            </button>
                                    @endif
                            </div>
                        

                        @if (!$loop->last && !$isItemCreationDisabled && !$isItemMovementDisabled)
                            <div x-show="isCreateButtonVisible" x-transition
                                class="absolute inset-x-0 bottom-6 z-20 flex items-center justify-center h-12 -mb-12">
                                {{--   <x-forms::dropdown>
                                    <x-slot name="trigger">
                                        <x-forms::icon-button :label="$getCreateItemBetweenButtonLabel()" icon="heroicon-o-plus" />
                                    </x-slot>

                                    <x-forms::builder.block-picker :blocks="$getBlocks()" :create-after-item="$uuid"
                                        :state-path="$getStatePath()" />
                                </x-forms::dropdown> --}}
                                {{--<x-filament::button type="button"
                                    x-on:click="$dispatch('open-modal', {id: 'filament-builder-{{ $getStatePath() }}',createAfterItem:'{{$uuid}}'})">
                                    {{ $getCreateItemButtonLabel() }}</x-filament::button>--}}
                                    <x-forms::icon-button x-on:click="$dispatch('open-modal', {id: 'filament-builder-{{ $getStatePath() }}',createAfterItem:'{{$uuid}}'})" :label="$getCreateItemBetweenButtonLabel()" class="border border-primary-500 bg-white hover:!bg-white" icon="heroicon-o-plus" />
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif

        @if (!$isItemCreationDisabled)
            <x-filament::button type="button"
                x-on:click="$dispatch('open-modal', {id: 'filament-builder-{{ $getStatePath() }}'})">
                {{ $getCreateItemButtonLabel() }}</x-filament::button>
        @endif
    </div>
    <x-filament::modal width="7xl" id="filament-builder-{{ $getStatePath() }}"  x-data="{createAfterItem:''}"
    x-on:open-modal.window="createAfterItem= $event.detail.createAfterItem || ''">
    
        <x-filament-custom-builder::block-picker :groups="$getBlocksByGroup()" :state-path="$getStatePath()" color="primary"/>
    </x-filament::modal>
</x-dynamic-component>
