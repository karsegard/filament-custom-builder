@props(['groups', 'statePath', 'color' => 'primary'])
{{-- blade-formatter-disable --}}
@php
    $iconClasses = \Illuminate\Support\Arr::toCssClasses([
            'filament-dropdown-list-item-icon mr-2 h-10 w-10 rtl:ml-2 rtl:mr-0', 
            'group-hover:text-white group-focus:text-white', 
            'group-hover:text-primary-500 group-focus:text-primary-500' => $color === 'primary' || $color === 'secondary',
            'text-danger-500' => $color === 'danger',
            'text-success-500' => $color === 'success', 
            'text-warning-500' => $color === 'warning'
        ]);
    $detailClasses = \Illuminate\Support\Arr::toCssClasses([
        'filament-dropdown-list-item-detail ml-auto text-xs text-gray-500', 
        'group-hover:text-primary-100 group-focus:text-primary-100' => $color === 'primary' || $color === 'secondary',
        'group-hover:text-danger-100 group-focus:text-danger-100' => $color === 'danger', 
        'group-hover:text-success-100 group-focus:text-success-100' => $color === 'success',
        'group-hover:text-warning-100 group-focus:text-warning-100' => $color === 'warning'
    ]);
@endphp
{{-- blade-formatter-enable --}}
<div {{ $attributes->class(['']) }}>
    
    @foreach ($groups as $group=>$blocks)
    <div class="mt-4">
        <span class="font-bold ">{{$group}}</span>
        <div class="grid grid-cols-4 gap-4 p-2">
    @foreach ($blocks as $block)
    @php
        $hint = method_exists($block,'getHint') ? $block->getHint():null;
    @endphp
        <div class="group min-h-full cursor-pointer border-2 rounded rounded-xl hover:border-primary-900 focus:border-primary-500 p-4 flex flex-col justify-between"
             x-on:click="$wire.dispatchFormEvent('builder::createItem', '{{ $statePath }}', '{{ $block->getName() }}',createAfterItem);close()">
             <div class="flex flex-row">
               @if ($block->getIcon())
                    <div
                         @class([
                             'filament-dropdown-list-item-icon mr-2 h-10 w-10 rtl:ml-2 rtl:mr-0',
                             'text-gray-500',
                             'group-hover:text-primary-500 group-focus:text-primary-500' =>
                                 $color === 'primary' || $color === 'secondary',
                             'text-danger-500' => $color === 'danger',
                             'text-success-500' => $color === 'success',
                             'text-warning-500' => $color === 'warning',
                         ])>
                        <x-dynamic-component
                                             :component="$block->getIcon()" />
                    </div>
                @endif
            <div
                 @class(['flex flex-col justify-center' => $block->getIcon()])>

                <div @class([
                    'filament-dropdown-list-item-detail  text-sm text-bold',
                    'group-hover:text-primary-500 group-focus:text-primary-500' =>
                        $color === 'primary' || $color === 'secondary',
                    'group-hover:text-danger-500 group-focus:text-danger-500' =>
                        $color === 'danger',
                    'group-hover:text-success-500 group-focus:text-success-500' =>
                        $color === 'success',
                    'group-hover:text-warning-500 group-focus:text-warning-500' =>
                        $color === 'warning',
                ])>{{ $block->getLabel() }}</div>

                @if(!blank($hint))
            <div class="text-gray-500 text-xs">{{$hint}}</div>
            @endif
            </div>
          
            </div>
        </div>
    @endforeach
    </div>
</div>
    @endforeach
</div>
